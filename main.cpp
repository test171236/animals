#include <cstdlib>

#include "animal.h"

int main()
{
    animal* zoo[10]{nullptr};

    for (animal* &an : zoo)
        switch(std::rand() % 3)
        {
        case 0:
            an = new dog();
            break;
        case 1:
            an = new cat();
            break;
        case 2:
            an = new cow();
            break;
        }

    for (animal*& an : zoo)
        an->Voice();

    for (animal*& an : zoo)
        delete an;

    return 0;
}