#pragma once

class animal
{
public:
    virtual void Voice() const = 0;
};

class dog : public animal
{
public:
    void Voice() const override;
};

class cat : public animal
{
public:
    void Voice() const override;
};

class cow : public animal
{
public:
    void Voice() const override;
};