#include <iostream>

#include "animal.h"

void dog::Voice() const
{
    std::cout << "Woof!\n";
}

void cat::Voice() const
{
    std::cout << "Meow!\n";
}

void cow::Voice() const
{
    std::cout << "Moo!\n";
}
